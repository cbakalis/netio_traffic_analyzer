#!/bin/sh                                                                                                                                                                                     

# sets environmental variables necessary to run the scripts

# only used in 191, otherwise ignore
export FLX_HOST_DAQ='pcatlnswfelix09'

# sets old software location by default
export OLD_SW="TRUE"

# please run this while in the netio_traffic_analyzer directory     
export REPO_LOC="${PWD}"                                                                                                          

#export SETUP="VS"
export SETUP="BB5"
#export SETUP="BB5_RS"
#export SETUP="B191_MM"

export SECTOR='A13'

export PHASE='PHASE2'

if [[ $SETUP == "VS" ]]; then
  # VS
  export FLX_HOST='pcatlnswfelix01'
  export SBC_HOST='sbcnsw-ttc-01'
  export JSON_PATH_PHASE1="$REPO_LOC/conf_lib/vs_internalPulser.json"
  export JSON_PATH_PHASE2="$REPO_LOC/conf_lib/vs_internalPulser.json"
  export OPC_XML_PATH='/opt/OpcUaScaServer/bin/2l1ddc_8mmfe8.xml'
  export ALTI_SLOT='9'

elif [[ $SETUP == "BB5" ]]; then
  # BB5
  export FLX_HOST='pcatlnswfelix04'
  export SBC_HOST='sbcatlnswbb1'
  export JSON_PATH_PHASE1='/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/BB5/Defaults/full_small_sector_default_bb5_internalPulser.json'
  export JSON_PATH_PHASE2='/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/BB5/Defaults/full_small_sector_default_bb5_internalPulser_phase2.json'
  export TEMPLATE_PATH="/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/BB5/Defaults/dont_touch/full_small_sector_default_bb5_internalPulser.json"
  export OPC_XML_PATH='/opt/OpcUaScaServer/bin/sector_A13_Uncalibration_MMG_BB5_NoADDC.xml'
  export ALTI_SLOT='11'

elif [[ $SETUP == "BB5_RS" ]]; then
  # BB5 Rotation Station
  export FLX_HOST='pcatlnswfelix02'
  export SBC_HOST='sbcatlnswbb1'
  export JSON_PATH_PHASE1='/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/BB5/Defaults/rotation_default_bb5_internalPulser.json'
  export TEMPLATE_PATH="/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/BB5/Defaults/dont_touch/rotation_default_bb5_internalPulser.json"
  export OPC_XML_PATH='/opt/OpcUaScaServer/bin/sector_ROTATION_MMG_BB5.xml'
  export ALTI_SLOT='15'

elif [[ $SETUP == "B191_MM" ]]; then
  # 191
  export FLX_HOST='pcatlnswfelix06'
  export SBC_HOST='sbcl1ct-191'
  export JSON_PATH_PHASE1='/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/Defaults/full_small_sector_default_191_internalPulser.json'
  export JSON_PATH_PHASE2='/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/Defaults/full_small_sector_default_191_internalPulser_phase2.json'
  export TEMPLATE_PATH="/afs/cern.ch/user/n/nswdaq/public/sw/config-ttc/config-files/config_json/191/A14/full_small_sector_a14_191_internalPulser.json"
  export OPC_XML_PATH='/opt/OpcUaScaServer/bin/sector_A14_Calibration_MMG_191.xml'
  export ALTI_SLOT='8'

fi

# probably these never need to be changed
export DUMPS_LOC="$REPO_LOC/dumps"
export BACKUP_LOC="$REPO_LOC/backup_logs"
export PYTHON_LOC="$REPO_LOC/python/netioDumpAnalyzer.py"
export SHELL_LOC="$REPO_LOC/shell_lib"
export LOG_LOC="$REPO_LOC/log.txt"
export CONF_APP='/afs/cern.ch/work/n/nswdaq/public/tdaq-08-03-01/sw/config-ttc/x86_64-centos7-gcc8-opt/NSWConfiguration/configure_frontend'
export SETUP_SCRIPT='/afs/cern.ch/work/n/nswdaq/public/tdaq-08-03-01/sw/setup.sh'
export OPCUA_LOC='/opt/OpcUaScaServer/bin/OpcUaScaServer'

