# ---------------------------------------------------------------------
# Usage: $ python phaseShifterROC.py <MMFE8_JSON>.json
# The python file and the .json should be in the same folder
# execute if you want to change the ROC PLL phase of a certain board

# 2020/02/22
# Christos Bakalis (christos.bakalis@cern.ch)
# ---------------------------------------------------------------------

from sys import argv
from tempfile import mkstemp
from shutil import move, copymode
import os
import fileinput
import re
import sys
from mmfe8_dict_phase1 import elinkList_phase1
from mmfe8_dict_phase2 import elinkList_phase2
import nta_tools as ntat

script, inputJson, outputJson = argv
tempFile = "tmp.json"
prompt = "> "

rocPLL40regAddr     = "reg115"
rocPLL40regNames    = ["ePllPhase40MHz_0", "ePllPhase40MHz_1", "ePllPhase40MHz_2"]
rocPLL160regNames   = ["ePllPhase160MHz_0[3:0]", "ePllPhase160MHz_1[3:0]", "ePllPhase160MHz_2[3:0]"]

# ------------------------------------------------
# ------------------------------------------------
# ------------------------------------------------
def main():
    boardPos    = 0
    posFound    = False 
    regPos      = 0
    oldPhase40  = 0

    oldPhase160 = 0

    phaseInput  = 0
    newPhase40  = 0
    newPhase160 = 0

    posOfAll40  = []
    posOfAll160 = []

    allBoards = []
    allBoards = ntat.getBoards(elinkList_phase1)

    allNonDefaultBoards = []
    allNonDefaultPhases = []

    # first get all the boards with non-default phase values in the input Json
    for currBrd in range(0, len(allBoards)):
        posFound, boardPos = ntat.get_matchLine(0, inputJson, allBoards[currBrd]) # first find the board
        ntat.notFound("Board", posFound) # check

        posFound, regPos = ntat.get_matchLine(boardPos, inputJson, rocPLL40regAddr) # then find the 40MHz register
        ntat.notFound("Register", posFound) # check

        posFound, phase40 = ntat.get_phaseVal(regPos, inputJson, rocPLL40regNames[0]) # then get the value of the register
        ntat.notFound("Phase Register", posFound) # check

        if(phase40 != 124):
            allNonDefaultBoards.append(allBoards[currBrd])
            allNonDefaultPhases.append(phase40)

    # then go through all boards at the output Json and change the values
    for nonDefaultBrd in range(0, len(allNonDefaultBoards)):
        posFound, boardPos = ntat.get_matchLine(0, outputJson, str(allNonDefaultBoards[nonDefaultBrd])) # first find the board
        ntat.notFound("Board", posFound) # check

        posFound, regPos = ntat.get_matchLine(boardPos, outputJson, rocPLL40regAddr) # then find the 40MHz register
        ntat.notFound("Register", posFound) # check

        posFound, oldPhase40 = ntat.get_phaseVal(regPos, outputJson, rocPLL40regNames[0]) # then get the value of the register
        ntat.notFound("Phase Register", posFound) # check

        newPhase40 = int(allNonDefaultPhases[nonDefaultBrd])

        newPhase160 = newPhase40 & 15
        oldPhase160 = oldPhase40 & 15
    
        # get all 40 and 160MHz register positions
        for index in range (0, len(rocPLL40regNames)):
            tmpPos   = 0
            tmpFound = False

            tmpFound, tmpPos = ntat.get_matchLine(regPos, outputJson, rocPLL40regNames[index])
            ntat.notFound("Phase Register", tmpFound) # check
            posOfAll40.append(tmpPos)

            tmpFound, tmpPos = ntat.get_matchLine(regPos, outputJson, rocPLL160regNames[index])
            ntat.notFound("Phase Register", tmpFound) # check
            posOfAll160.append(tmpPos)

    ntat.change_oldPhases(posOfAll40, posOfAll160, ":"+str(oldPhase40), ":"+str(newPhase40), ":"+str(oldPhase160), ":"+str(newPhase160), outputJson, tempFile)

if __name__ == '__main__':
    main()


