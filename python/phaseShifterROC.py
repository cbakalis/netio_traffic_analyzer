# ---------------------------------------------------------------------
# Usage: $ python phaseShifterROC.py <MMFE8_JSON>.json
# The python file and the .json should be in the same folder
# execute if you want to change the ROC PLL phase of a certain board

# 2020/02/22
# Christos Bakalis (christos.bakalis@cern.ch)
# ---------------------------------------------------------------------

from sys import argv
from tempfile import mkstemp
from shutil import move, copymode
import os
import fileinput
import re
import sys
import nta_tools as ntat

script, inputJson = argv
tempFile = "tmp.json"
prompt = "> "

rocPLL40regAddr     = "reg115"
rocPLL40regNames    = ["ePllPhase40MHz_0", "ePllPhase40MHz_1", "ePllPhase40MHz_2"]
rocPLL160regNames   = ["ePllPhase160MHz_0[3:0]", "ePllPhase160MHz_1[3:0]", "ePllPhase160MHz_2[3:0]"]

# ------------------------------------------------
# ------------------------------------------------
# ------------------------------------------------
def main():
    boardPos    = 0
    posFound    = False 
    regPos      = 0
    oldPhase40  = 0

    oldPhase160 = 0

    phaseInput  = 0
    newPhase40  = 0
    newPhase160 = 0

    posOfAll40  = []
    posOfAll160 = []

    print("Please choose the board that you wish to change its core ROC clock phase. (Example input = L1P1_IPL)")
    boardChoice = input(prompt)

    posFound, boardPos = ntat.get_matchLine(0, inputJson, boardChoice) # first find the board
    ntat.notFound("Board", posFound) # check

    posFound, regPos = ntat.get_matchLine(boardPos, inputJson, rocPLL40regAddr) # then find the 40MHz register
    ntat.notFound("Register", posFound) # check

    posFound, oldPhase40 = ntat.get_phaseVal(regPos, inputJson, rocPLL40regNames[0]) # then get the value of the register
    ntat.notFound("Phase Register", posFound) # check

    print("The current 40MHz clock phase value is", oldPhase40, "(default = 124)")
    print("What do you want to change it to? (suggested is 120 or 116. First try 120, then try 116 if 120 fails)")
    phaseInput = input(prompt)

    newPhase40 = int(phaseInput)

    if(newPhase40 < 0 or newPhase40 > 127):
        print("Please give a valid 7-bit value to the register")
        exit(1)

    newPhase160 = newPhase40 & 15
    oldPhase160 = oldPhase40 & 15

    # get all 40 and 160MHz register positions
    for index in range (0, len(rocPLL40regNames)):
        tmpPos   = 0
        tmpFound = False

        tmpFound, tmpPos = ntat.get_matchLine(regPos, inputJson, rocPLL40regNames[index])
        ntat.notFound("Phase Register", tmpFound) # check
        posOfAll40.append(tmpPos)

        tmpFound, tmpPos = ntat.get_matchLine(regPos, inputJson, rocPLL160regNames[index])
        ntat.notFound("Phase Register", tmpFound) # check
        posOfAll160.append(tmpPos)

    ntat.change_oldPhases(posOfAll40, posOfAll160, ":"+str(oldPhase40), ":"+str(newPhase40), ":"+str(oldPhase160), ":"+str(newPhase160), inputJson, tempFile)


if __name__ == '__main__':
    main()


