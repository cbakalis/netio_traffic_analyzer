# cb
import math
import sys
#import matplotlib.pyplot as plt
#import numpy as np
import re
import datetime
import os
#from matplotlib.ticker import MaxNLocator

phase = os.environ['PHASE']
setup = os.environ['SETUP']

if(setup == "BB5_RS" or setup == "BB5"):
    from mmfe8_dict_phase1 import elinkList_phase1
    from mmfe8_dict_phase2 import elinkList_phase2
else:
    from mmfe8_dict_191_phase1 import elinkList_phase1
    from mmfe8_dict_191_phase2 import elinkList_phase2

# Phase-I
glbl_entries_OK   = 1000 # for health evaluation
glbl_entries_err  = 5    # for health evaluation
glbl_sizeFull_A   = 2000 # for health evaluation, 2064 is full
glbl_sizeFull_B   = 1800 # for health evaluation, 2064 is full
glbl_sizeHalf_A   = 900  # for health evaluation, ~1050 is full
glbl_sizeHalf_B   = 750  # for health evaluation, ~1050 is full

# Phase-II
glbl_size1vmm_A   = 260
glbl_size1vmm_B   = 180

glbl_size2vmm_A   = 500
glbl_size2vmm_B   = 400

glbl_size3vmm_A   = 760
glbl_size3vmm_B   = 600

glbl_size5vmm_A   = 1260
glbl_size5vmm_B   = 1000

phase2_maxEntries = 1000


allInputArgs = sys.argv
# syntax: $ python netioDumpAnalyzer.py <linkNumber> <dumps>
# e.g. $ python netioDumpAnalyzer.py 12 dump_80.txt dump_88.txt dump_827.txt
linkNumber = sys.argv[1] # 0 = python script, 1 = linkNumber

# -----------------------------------------
# Checks if the file is empty and writes 
# one size=0 entry
# -----------------------------------------
def check_file(inputFile):
    if(os.stat(inputFile).st_size == 0):
        myFile = open(inputFile, 'a')
        myFile.write(">>> size=0")
        myFile.close()

# -----------------------------------------
# Gets the message size from a single line
# -----------------------------------------
def get_msgSize(messageLine):
    msgSize = 0 # for the loop
    index   = len(messageLine) # for the loop

    while(index >= 1):
        if(messageLine[index - 1 : index] == "="): # find the 'equal' sign
            msgSize = int(messageLine[index : len(messageLine)]) # grab and cast everything after the equal
            break

        index = index - 1
    return msgSize

# ----------------------------------------------
# Scans through all lines of an input dump file
# and returns all the size values in an array
# ----------------------------------------------
def fill_sizeArray(dumpFileInput):
    sizeArray = []

    for line in dumpFileInput:
        line_length = len(line)
        if (line[0:3] == ">>>"):
            sizeArray.append(get_msgSize(line))

    return sizeArray

# -------------------------------------------
# Scans through all input files and returns
# all size values of the files in a 2D array
# -------------------------------------------
def get_allSizes(dumpFiles):
    arrayOfSizes    = []
    singleSizeArray = []

    for fileIndex in range (2, len(dumpFiles)):  # the first argument is the python file itself, the second is the link
        check_file(sys.argv[fileIndex]) # check first and write into if necessary
        textFile = open(sys.argv[fileIndex], 'r')
        singleSizeArray = fill_sizeArray(textFile) # first get the size of the current dump file
        arrayOfSizes.append(singleSizeArray) # then append it to the array

    return arrayOfSizes

# ----------------------------------
# Gets the E-link ID of a dump file
# ----------------------------------
def get_elinkId(dumpFileInput):
    fullStr   = dumpFileInput.name
    elinkStr  = re.findall('\d+', fullStr) # get all digits and 'append' them together
    return int(elinkStr[len(elinkStr)-1]) # the last one yields the dump number


# ---------------------------------
# Scans through all input files 
# and returns all E-link ID values
# ---------------------------------
def get_allElinkIDs(dumpFiles):
    arrayOfElinkIDs = []
    elinkID         = 0

    for fileIndex in range (2, len(dumpFiles)):  # the first argument is the python file itself, the second is the link
        textFile = open(sys.argv[fileIndex], 'r')
        elinkID = get_elinkId(textFile)
        arrayOfElinkIDs.append(elinkID)

    return arrayOfElinkIDs

# ----------------
# Gets statistics
# ----------------
def get_arrayStats(sizeArray):
    stdDev = 0
    mean   = 0
    minimum = sizeArray[0]
    maximum = 0
    entries = len(sizeArray)
    sizeSum = 0
    sdevSum = 0

    for i in range (0, entries):
        sizeSum = sizeSum + sizeArray[i]

        if(sizeArray[i] > maximum):
            maximum = sizeArray[i]

        if(sizeArray[i] < minimum):
            minimum = sizeArray[i]

    mean = sizeSum/entries

    for i in range (0, entries):
        sdevSum = (sizeArray[i] - mean)**2 + sdevSum

    stdDev = (sdevSum/entries)**0.5

    return round(stdDev,3), round(mean, 3), minimum, maximum, entries

# --------------------------------------------
# Health Evaluator for Phase-I. Gets E-link ID  
# and statistics and evaluates DAQ health
# --------------------------------------------
def eval_health_phase1(elinkID, sizes, totalEntries):
    entries_OK  = totalEntries
    entries_err = glbl_entries_err   
    size_A      = glbl_sizeFull_A # full
    size_B      = glbl_sizeFull_B # full

    healthInt = 0 # 0 = bad, 1 = meh, 2 = good, 3 = perfect
    elink_str = elinkList_phase1[elinkID]

    if(elink_str[len(elink_str) - 1] == 'A' or elink_str[len(elink_str) - 1] == 'B'): # is it a 'half' board?
        size_A = glbl_sizeHalf_A
        size_B = glbl_sizeHalf_B
    elif(elink_str[len(elink_str) - 1] == 'O'): #L1AINFO
        size_A = 28

    stdDev, mean, minimum, maximum, entries = get_arrayStats(sizes)

    if(entries == 0 or (entries > 0 and entries < 0 + entries_err)): # too few
        healthInt = 0

    elif(entries > 0 + entries_err and entries < entries_OK - entries_err): # not enough
        healthInt = 0

    elif(entries > entries_OK + entries_err): # too many
        healthInt = 0 

    else: # enough entries. lets see the stdDev and the mean
        if(mean > 0 and mean < size_B and mean < size_A): # not very good
            healthInt = 1
        elif(mean > size_B and mean < size_A): # very good
            healthInt = 2
        else:
            healthInt = 3 # perfect

    return healthInt

# --------------------------------------------
# Health Evaluator for Phase-II. Gets E-link ID  
# and statistics and evaluates DAQ health
# --------------------------------------------
def eval_health_phase2(elinkID, sizes, totalEntries):
    entries_OK  = totalEntries
    entries_err = glbl_entries_err   
    size_A      = 0
    size_B      = 0

    healthInt   = 0 # 0 = bad, 1 = meh, 2 = good, 3 = perfect
    elink_str = elinkList_phase2[elinkID]

    # PCBs 1-5
    if( elink_str[len(elink_str) - 7] == '1' or 
        elink_str[len(elink_str) - 7] == '2' or 
        elink_str[len(elink_str) - 7] == '3' or 
        elink_str[len(elink_str) - 7] == '4' or 
        elink_str[len(elink_str) - 7] == '5'): # sROCs of first five PCBs have 2 VMMs each
        size_A = glbl_size2vmm_A
        size_B = glbl_size2vmm_B

    # PCB 6
    elif(elink_str[len(elink_str) - 7] == '6' and elink_str[len(elink_str) - 1] == 'C'): # last sROC of PCB6 has 2 VMMs
        size_A = glbl_size2vmm_A
        size_B = glbl_size2vmm_B
    elif(elink_str[len(elink_str) - 7] == '6' and elink_str[len(elink_str) - 1] != 'C'): # two first sROCs of PCB6 have 3 VMMs
        size_A = glbl_size3vmm_A
        size_B = glbl_size3vmm_B

    # PCB 7
    elif(elink_str[len(elink_str) - 7] == '7' and elink_str[len(elink_str) - 1] == 'A'): # first two sROCs of PCB7 have 1 VMM
        size_A = glbl_size1vmm_A
        size_B = glbl_size1vmm_B
    elif(elink_str[len(elink_str) - 7] == '7' and elink_str[len(elink_str) - 1] == 'B'): # first two sROCs of PCB7 have 1 VMM
        size_A = glbl_size1vmm_A
        size_B = glbl_size1vmm_B
    elif(elink_str[len(elink_str) - 7] == '7' and elink_str[len(elink_str) - 1] == 'C'): # last two sROCs of PCB7 have 3 VMMs
        size_A = glbl_size3vmm_A
        size_B = glbl_size3vmm_B
    elif(elink_str[len(elink_str) - 7] == '7' and elink_str[len(elink_str) - 1] == 'D'): # last two sROCs of PCB7 have 3 VMMs
        size_A = glbl_size3vmm_A
        size_B = glbl_size3vmm_B

    # PCB 8
    elif(elink_str[len(elink_str) - 7] == '8' and elink_str[len(elink_str) - 1] == 'D'): # last sROC of PCB8 has 5 VMMs
        size_A = glbl_size5vmm_A
        size_B = glbl_size5vmm_B
    elif(elink_str[len(elink_str) - 7] == '8' and elink_str[len(elink_str) - 1] != 'D'): # three first sROCs of PCB6 have 1 VMM
        size_A = glbl_size1vmm_A
        size_B = glbl_size1vmm_B

    if(elinkID == 827):
        size_A = 28
        size_B = 0

    stdDev, mean, minimum, maximum, entries = get_arrayStats(sizes)

    # time to evaluate
    if(entries == 0 or (entries > 0 and entries < 0 + entries_err)): # too few
        healthInt = 0

    elif(entries > 0 + entries_err and entries < entries_OK - entries_err): # not enough
        healthInt = 0

    elif(entries > entries_OK + entries_err): # too many
        healthInt = 0 

    elif(mean > 1 and mean < size_B and mean < size_A and mean < glbl_sizeFull_A): # not very good
        healthInt = 1
    elif(mean > size_B and mean < size_A and mean < glbl_sizeFull_A): # very good
        healthInt = 2
    else:
        healthInt = 3 # perfect

    return healthInt

def conv_healthIntToString(healthInt):
    if(healthInt == 3):
        return 'A+'
    elif(healthInt == 2):
        return 'A'
    elif(healthInt == 1):
        return 'B'
    else:
        return 'F'

# -------------------------------------------
# Gets number of Events from L1ID Info E-link
# -------------------------------------------
def get_numberOfEvents(allElinkIDs, allSizes):
    totalDumps = len(allElinkIDs)

    for dumpIndex in range(0, totalDumps):
        if(allElinkIDs[dumpIndex] == 827):
            stdDev_l1IDinfo, mean_l1IDinfo, minimum_l1IDinfo, maximum_l1IDinfo, entries_l1IDinfo = get_arrayStats(allSizes[dumpIndex])
            break

    return entries_l1IDinfo

# ------------------
# Prints statistics
# ------------------
def print_stats(allElinkIDs, allSizes, entries_l1IDinfo):
    log = open("log.txt", 'a')
    totalDumps = len(allElinkIDs)
    strLink    = "---    L1DDC Link = " + str(linkNumber) + "   ---"

    log.write("\n")
    log.write("----------------------------\n")
    log.write(str(datetime.datetime.now()))
    log.write("\n")
    log.write("----------------------------\n")
    if(phase == "PHASE2"):
        log.write("----------------------------\n")
        log.write("- Phase-II E-links Testing -\n")
        log.write("----------------------------\n")
    log.write("\n")
    log.write("----------------------------\n")
    log.write(str(strLink))
    log.write("\n")
    log.write("----------------------------\n")
    log.write("\n")

    for dumpIndex in range(0, totalDumps):
        stdDev, mean, minimum, maximum, entries = get_arrayStats(allSizes[dumpIndex])

        strID       = "E-link ID = " + str(allElinkIDs[dumpIndex])
        
        if(phase == "PHASE1"):
            strMMFE8    = "        MMFE8 = " + str(elinkList_phase1[allElinkIDs[dumpIndex]])
            strHealth   = "        Health = " + str(conv_healthIntToString(eval_health_phase1(allElinkIDs[dumpIndex], allSizes[dumpIndex], entries_l1IDinfo)))
        else:
            strMMFE8    = "        MMFE8 = " + str(elinkList_phase2[allElinkIDs[dumpIndex]])
            strHealth   = "        Health = " + str(conv_healthIntToString(eval_health_phase2(allElinkIDs[dumpIndex], allSizes[dumpIndex], entries_l1IDinfo)))
            
        strEntries  = "        Events = " + str(entries)
        strMeanSize = "        Mean Size = " + str(mean)
        strSdev     = "        Sdev = " + str(stdDev)

        print(strID),
        print(strMMFE8),
        print(strHealth),
        print(strEntries),
        print(strMeanSize),
        print(strSdev)

        log.write(strID)
        log.write(strMMFE8)
        log.write(strHealth)
        log.write(strEntries)
        log.write(strMeanSize)
        log.write(strSdev)
        log.write("\n")

    log.write("\n")
    log.write("----------------------------\n")
    log.write("-----------  END  ----------\n")
    log.write("----------------------------\n")
    log.write("\n")
    log.write("\n")

    log.close()

# ------------------------------------------------
# ------------------------------------------------
# ------------------------------------------------
def main():
    print("analyzing...")
    totalEntries = get_numberOfEvents(get_allElinkIDs(allInputArgs), get_allSizes(allInputArgs))
    print_stats(get_allElinkIDs(allInputArgs), get_allSizes(allInputArgs), totalEntries)
    print("done")

if __name__ == '__main__':
    main()
