# ---------------------------------------------------------------------
# Usage: $ python phaseFinderROC.py <MMFE8_JSON>.json
# Lists the 40 MHz ROC core clock phase values of all MMFE8s 
# in a .json file

# 2020/02/26
# Christos Bakalis (christos.bakalis@cern.ch)
# ---------------------------------------------------------------------

from sys import argv
from tempfile import mkstemp
from shutil import move, copymode
import os
import fileinput
import re
import sys
import datetime
from mmfe8_dict_phase1 import elinkList_phase1
from mmfe8_dict_phase2 import elinkList_phase2
import nta_tools as ntat

script, inputJson = argv
backupLoc = os.environ['BACKUP_LOC']
sector = os.environ['SECTOR']

rocPLL40regAddr     = "reg115"
rocPLL40regName     = "ePllPhase40MHz_0"

# ------------------------------------------------
# ------------------------------------------------
# ------------------------------------------------
def main():
    boardPos  = 0
    posFound  = False 
    regPos    = 0
    phase40   = 0

    allBoards = []
    allBoards = ntat.getBoards(elinkList_phase1)

    allNonDefaultBoards = []

    logFileLoc = backupLoc + "/MMFE8_ROCclk_phaseList_" + str(sector) + ".txt"

    if os.path.isfile(logFileLoc): # delete it
        os.remove(logFileLoc)

    log = open(logFileLoc, 'a')

    for currBrd in range(0, len(allBoards)):
        posFound, boardPos = ntat.get_matchLine(0, inputJson, allBoards[currBrd]) # first find the board
        ntat.notFound("Board", posFound) # check

        posFound, regPos = ntat.get_matchLine(boardPos, inputJson, rocPLL40regAddr) # then find the 40MHz register
        ntat.notFound("Register", posFound) # check

        posFound, phase40 = ntat.get_phaseVal(regPos, inputJson, rocPLL40regName) # then get the value of the register
        ntat.notFound("Phase Register", posFound) # check

        if(phase40 != 124):
            allNonDefaultBoards.append(allBoards[currBrd])

        strToWrite = "MMFE8 = " + str(allBoards[currBrd]) + " 40MHz Phase = " + str(phase40) + "\n"
        log.write(strToWrite)


    defaultStr = "Number of non-Default Boards = " + str(len(allNonDefaultBoards)) + "\n"
    log.write("------------------------------------\n")
    log.write((str(datetime.datetime.now())))
    log.write("\n")
    log.write("------------------------------------\n")
    log.write(defaultStr)
    log.write("------------------------------------\n")
    log.close()

if __name__ == '__main__':
    main()


