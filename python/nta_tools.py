from sys import argv
from tempfile import mkstemp
from shutil import move, copymode
import os
import fileinput
import re
import sys
import datetime

# ----------------------------------------------
# Checks if a boolean is true, and prints
# an error & exits if it is false
# ----------------------------------------------
def notFound(whatWasNotFound, boolToCheck):
    if(boolToCheck == False):
        print(whatWasNotFound,"not found...")
        exit(1)

# ----------------------------------------------
# Gets all MMFE8s and stores them in a vector
# ----------------------------------------------
def getBoards(elinkList):
    skipNext = False
    boardsAll = []

    for key, value in elinkList.items():
        if(skipNext == True):
            skipNext = False
        else:
            board = elinkList[key]
            strSize = len(board)
            if(strSize == 10): # split in two
                boardFinal = board[0:8]
                skipNext = True
            else:
                boardFinal = board

            if(key != 827): # skip the L1A_INFO
                boardsAll.append(boardFinal)

    return boardsAll


# ----------------------------------------------
# Scans through all lines of an input file and
# returns if a string was found and its line.
# will break on first match
# ----------------------------------------------
def get_matchLine(startFrom, inputFile, stringToSearch):
    linePos     = 0
    boardFound  = False

    myFile = open(inputFile, 'r')

    for line in myFile.readlines():
        linePos += 1
        if(stringToSearch in line): # found it. break
            if(linePos > startFrom): # is it the first occurence after our start position?
                boardFound = True
                break

    myFile.close()
    return boardFound, linePos

# --------------------------------------
# Gets the ROC PLL phase register value
# --------------------------------------
def get_phaseVal(startFrom, inputFile, regName):
    linePos   = 0
    pllPhase  = 0
    posFound  = False

    myFile = open(inputFile, 'r')

    for line in myFile.readlines():
        linePos += 1
        if(regName in line):
            if(linePos >= startFrom):
                pllPhase   = re.findall('\:(.*)', line) # get everything after the ':' and append them together
                posFound = True
                break

    myFile.close()
    return posFound, int(pllPhase[0])


# --------------------------------------
# Provide this with the lines that contain
# the values that you want to change
# --------------------------------------
def change_oldPhases(all40, all160, old40, new40, old160, new160, inputFile, temporaryFile):
    linePos     = 0
    inFile      = open(inputFile, 'r')
    tmpFile     = open(temporaryFile, 'w+')
    currPos40   = 0
    currPos160  = 0

    for line in inFile.readlines():
        linePos += 1

        if(currPos40 < len(all40) and linePos == all40[currPos40]):
            newline = line.replace(old40, new40)
            tmpFile.write(newline)
            currPos40 += 1
        elif(currPos160 < len(all160) and linePos == all160[currPos160]):
            newline = line.replace(old160, new160)
            tmpFile.write(newline)
            currPos160 += 1
        else:
            tmpFile.write(line)

    inFile.close()

    tmpFile.seek(0) #rewind
    inFile = open(inputFile, 'w+')

    for line in tmpFile: # overwrite
        inFile.write(line)  

    tmpFile.close()
    inFile.close()
    os.remove(temporaryFile)