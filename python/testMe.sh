#!/bin/sh


DUMPS_ARRAY=()

for entry in testDumps/dump_*.txt
do
  DUMPS_ARRAY+=($entry)
done

python3 netioDumpAnalyzer.py $(printf ' %s' "${DUMPS_ARRAY[@]}")

