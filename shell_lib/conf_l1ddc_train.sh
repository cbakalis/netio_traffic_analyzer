#!/bin/sh

# train. DEPRECATED!
BANK_1_TRN_REGS=( 102 103 104 )
BANK_2_3_4_TRN_REGS=( 126 127 128 150 151 152 174 175 176 )

# resets. NOW USED!
BANK_1_RST_REGS=( 108 109 110 )
BANK_2_3_4_RST_REGS=( 132 133 134 156 157 158 180 181 182 )
# tied with /afs/cern.ch/user/n/nswdaq/public/felix-sw/l1ddc_config_files/MM_L1DDC_PHASE1_320_GBTX1_auto.xml
# the .xml above has zero to all training regs, and 0xFF to all phase-aligner reset regs
# the loop below sets the reset to low

# CARD0 RST LOW
for link in {0..11}
do

  for regs in "${BANK_1_RST_REGS[@]}"
  do
    fice -d 0 -G $link -I 1 -a $regs 01
  done

  for regs in "${BANK_2_3_4_RST_REGS[@]}"
  do
    fice -d 0 -G $link -I 1 -a $regs 00
  done

  echo "Done L1DDC at link $link card 0"
  echo "Waiting..."
  sleep 1
done

# CARD1 RST LOW 191
if [[ $SETUP == *"191"* ]]; then

  for link in 6 7 9 10
  do
    
    for regs in "${BANK_1_RST_REGS[@]}"
    do
      fice -d 1 -G $link -I 1 -a $regs 01
    done

    for regs in "${BANK_2_3_4_RST_REGS[@]}"
    do
      fice -d 1 -G $link -I 1 -a $regs 00
    done

    echo "Done L1DDC at link $link card 1"
    echo "Waiting..."
    sleep 1
  done

# CARD1 RST LOW BB5 (add more if needed)
else

  for link in {0..3}
  do

    for regs in "${BANK_1_RST_REGS[@]}"
    do
      fice -d 1 -G $link -I 1 -a $regs 01
    done

    for regs in "${BANK_2_3_4_RST_REGS[@]}"
    do
      fice -d 1 -G $link -I 1 -a $regs 00
    done

    echo "Done L1DDC at link $link card 1"
    echo "Waiting..."
    sleep 1
  done

fi

# OLD. Used to have it LOW in the original .xml, then toggle from HIGH to LOW
# # CARD0 RST HIGH
# for link in {0..11}
# do

#   for regs in "${BANK_1_RST_REGS[@]}"
#   do
#     fice -d 0 -G $link -I 1 -a $regs ff
#   done

#   for regs in "${BANK_2_3_4_RST_REGS[@]}"
#   do
#     fice -d 0 -G $link -I 1 -a $regs ff
#   done

#   echo "Done L1DDC at link $link card 0"
#   echo "Waiting..."
#   sleep 1
# done

# # CARD1 RST HIGH 191
# if [[ $SETUP == *"191"* ]]; then

#   for link in 6 7 9 10
#   do
    
#     for regs in "${BANK_1_RST_REGS[@]}"
#     do
#       fice -d 1 -G $link -I 1 -a $regs ff
#     done

#     for regs in "${BANK_2_3_4_RST_REGS[@]}"
#     do
#       fice -d 1 -G $link -I 1 -a $regs ff
#     done

#     echo "Done L1DDC at link $link card 1"
#     echo "Waiting..."
#     sleep 1
#   done

# # CARD1 RST HIGH BB5 (add more if needed)
# else

#   for link in {0..3}
#   do

#     for regs in "${BANK_1_RST_REGS[@]}"
#     do
#       fice -d 1 -G $link -I 1 -a $regs ff
#     done

#     for regs in "${BANK_2_3_4_RST_REGS[@]}"
#     do
#       fice -d 1 -G $link -I 1 -a $regs ff
#     done

#     echo "Done L1DDC at link $link card 1"
#     echo "Waiting..."
#     sleep 1
#   done

# fi