#!/bin/sh
# first check if we have sourced what we are supposed to
if [[ "$(echo $LD_LIBRARY_PATH | grep -c felix)" -lt 1 ]]; then
    source /opt/felix/setup.sh
fi

# works at BB5
L1DDCS_CARD0_INIT=(3f 7f bf ff 13f 17f 1bf 1ff 23f 27f 2bf 2ff)

if [[ $SETUP == *"191"* ]]; then
  L1DDCS_CARD1_INIT=(9bf 9ff a7f abf)
else
  L1DDCS_CARD1_INIT=(83f 87f 8bf 8ff)
fi

L1DDCS_CARD0=()
L1DDCS_CARD1=()
BANKS_ADDR=( 84 85 86 108 109 110 132 133 134 156 157 158 180 181 182 )
FELIXADDR_CARD0="simple-netio://direct/$FLX_HOST.cern.ch/12340/12350"
FELIXADDR_CARD1="simple-netio://direct/$FLX_HOST.cern.ch/12341/12351"
TOOL_PATH="/afs/cern.ch/work/n/nswdaq/public/ScaSoftwareTools-master/gbtx_configuration"
I2C_TOOL_PATH="/afs/cern.ch/work/n/nswdaq/public/ScaSoftwareTools-master/standalone_i2c"
GBTX_CONF_PATH="/afs/cern.ch/user/n/nswdaq/public/felix-sw/l1ddc_config_files/GBTX2_MM_auto.txt"

if [ $# -eq 0 ]
  then
    L1DDCS_CARD0=(3f 7f bf ff 13f 17f 1bf 1ff 23f 27f 2bf 2ff)

    if [[ $SETUP == *"191"* ]]; then
      L1DDCS_CARD1=(9bf 9ff a7f abf)
    else
      L1DDCS_CARD1=(83f 87f 8bf 8ff)
    fi
    
  else
  if (( $1 < 12 )); then
    L1DDCS_CARD0[0]=${L1DDCS_CARD0_INIT[$1]}
  else
    L1DDCS_CARD1[0]=${L1DDCS_CARD1_INIT[$1-12]}
  fi  
fi


# now configuring all L1DDC SCAs

# first the entire address space for both cards
for sca_elink in "${L1DDCS_CARD0[@]}"
do
    $TOOL_PATH -a $FELIXADDR_CARD0/$sca_elink -i 0 -d 2 $GBTX_CONF_PATH
    $TOOL_PATH -a $FELIXADDR_CARD0/$sca_elink -i 1 -d 3 $GBTX_CONF_PATH
done

for sca_elink in "${L1DDCS_CARD1[@]}"
do
    $TOOL_PATH -a $FELIXADDR_CARD1/$sca_elink -i 0 -d 2 $GBTX_CONF_PATH
    $TOOL_PATH -a $FELIXADDR_CARD1/$sca_elink -i 1 -d 3 $GBTX_CONF_PATH
done

# now only the desired regs for both cards
for sca_elink in "${L1DDCS_CARD0[@]}"
do
    for reg in "${BANKS_ADDR[@]}"
    do
        $I2C_TOOL_PATH -a $FELIXADDR_CARD0/$sca_elink -i 0 -d 2 -g $reg -w 00
        $I2C_TOOL_PATH -a $FELIXADDR_CARD0/$sca_elink -i 1 -d 3 -g $reg -w 00
    done
done

for sca_elink in "${L1DDCS_CARD1[@]}"
do
    for reg in "${BANKS_ADDR[@]}"
    do
        $I2C_TOOL_PATH -a $FELIXADDR_CARD1/$sca_elink -i 0 -d 2 -g $reg -w 00
        $I2C_TOOL_PATH -a $FELIXADDR_CARD1/$sca_elink -i 1 -d 3 -g $reg -w 00
    done
done

