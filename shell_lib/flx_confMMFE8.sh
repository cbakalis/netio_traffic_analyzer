#!/bin/sh

source $SETUP_SCRIPT

if [[ $PHASE == "PHASE1" ]]; then
  $CONF_APP -c $JSON_PATH_PHASE1 -v -r --resetvmm
else
  $CONF_APP -c $JSON_PATH_PHASE2 -v -r --resetvmm
fi
