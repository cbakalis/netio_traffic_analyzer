
if [[ $SETUP == *"191"* ]]; then
  export ELC_CONF_DIR=/afs/cern.ch/user/n/nswdaq/public/felix-sw/elinkconfig_files/partial/elc_new320/191
else
  export ELC_CONF_DIR=/afs/cern.ch/user/n/nswdaq/public/felix-sw/elinkconfig_files/partial/elc_new320
fi   


for i in {0..1}
do
  echo "$(tput setaf 7)Running FELIX E-Link configuration(SCA+ROC) for device $i at $HOSTNAME __191__/MMG or BB5/MMG. $(tput sgr 0)"
  echo $ELC_CONF_DIR/link_$1.elc
  if [[ $1 -lt 12 ]]; then
    if [[ $i == 0 ]]; then
        feconf -d $i $ELC_CONF_DIR/link_$1.elc
    fi 

    if [[ $i == 1 ]]; then
        feconf -d $i $ELC_CONF_DIR/link_none.elc
    fi 

  else

    LINK_CARD1=$(( $1 - 12 ))

    if [[ $i == 0 ]]; then
        feconf -d $i $ELC_CONF_DIR/link_none.elc
    fi 

    if [[ $i == 1 ]]; then
        feconf -d $i $ELC_CONF_DIR/link_$LINK_CARD1.elc
    fi 
  fi

  echo "Done FELIX E-Link Phase-I configuration(SCA+ROC+TTC) for device $i"

done 
