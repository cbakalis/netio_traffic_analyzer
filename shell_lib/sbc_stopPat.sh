#!/bin/sh
source /afs/cern.ch/work/n/nswdaq/public/felix-sw/netio_traffic_analyzer/set_envVars.sh
source /afs/cern.ch/work/n/nswdaq/public/felix-sw/netio_traffic_analyzer/shell_lib/sbc_setup.sh || true

# look at me I am at the BB5 cosmic stand
if [ $SBC_HOST == *"sbcatlnswbb1"* ] && [ $FLX_HOST == *"pcatlnswfelix04"* ]; then
  /afs/cern.ch/user/n/nswdaq/public/alti/ALTI_disablePattern.expect $ALTI_SLOT || true

# look at me I am at the BB5 rotation station
elif [ $SBC_HOST == *"sbcatlnswbb1"* ] && [ $FLX_HOST == *"pcatlnswfelix02"* ]; then
/afs/cern.ch/user/n/nswdaq/public/alti/ALTI_disablePattern.expect $ALTI_SLOT || true

# look at me I am at the VS
elif [ $SBC_HOST == *"sbcnsw-ttc-01"* ]; then
  /afs/cern.ch/user/n/nswdaq/public/alti/ALTI_disablePattern.expect $ALTI_SLOT || true
fi


