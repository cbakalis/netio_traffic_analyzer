#!/bin/sh

if [[ $SETUP == *"191"* ]]; then
  export ELC_CONF_DIR=/afs/cern.ch/user/n/nswdaq/public/felix-sw/elinkconfig_files/phase2/191
else
  export ELC_CONF_DIR=/afs/cern.ch/user/n/nswdaq/public/felix-sw/elinkconfig_files/phase2
fi   


for i in {0..3}
do
  echo "$(tput setaf 7)Running Phase-II FELIX E-Link configuration(SCA+TTC) for device $i at $HOSTNAME for __191__/MMG or BB5/MMG. $(tput sgr 0)"
  feconf -d $i $ELC_CONF_DIR/link_$1_card$i.elc
  echo "Done Phase-II FELIX E-Link configuration(SCA+TTC) for device $i"
done
