#!/bin/sh

if [[ $PHASE == *"PHASE1"* ]]; then
  source shell_lib/elinks_phase1.sh $1 
elif [[ $PHASE == *"PHASE2"* ]]; then
  source shell_lib/elinks_phase2.sh $1 
fi
# add more hosts here

# cleanup
rm -f $DUMPS_LOC/dump_*.txt

# subscribe

# always subscribe to L1A-Info E-link 
netio_cat subscribe -H $FLX_HOST -e raw -p 12350 -t 827 > $DUMPS_LOC/dump_827.txt&

for i in "${BOARDS_CARD0[@]}"
do
  netio_cat subscribe -H $FLX_HOST -e raw -p 12350 -t $i > $DUMPS_LOC/dump_$i.txt&
done

for i in "${BOARDS_CARD1[@]}"
do
  netio_cat subscribe -H $FLX_HOST -e raw -p 12351 -t $i > $DUMPS_LOC/dump_$i.txt&
done

for i in "${BOARDS_CARD2[@]}"
do
  netio_cat subscribe -H $FLX_HOST -e raw -p 12352 -t $i > $DUMPS_LOC/dump_$i.txt&
done

for i in "${BOARDS_CARD3[@]}"
do
  netio_cat subscribe -H $FLX_HOST -e raw -p 12353 -t $i > $DUMPS_LOC/dump_$i.txt&
done
