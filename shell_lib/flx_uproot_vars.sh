#!/bin/sh

# removes variables set by the rpm-installed felix software

if [[ $OLD_SW = "TRUE" ]]; then
  echo "Removing the rpm-installed variables and sourcing the custom git-cloned software..."
  export PATH=$( echo -e ${PATH} | tr -s ":" "\n" | grep -vwE "(/opt/felix/bin)" | tr -s "\n" ":" | sed "s/:$//" )
  export QT_XKB_CONFIG_ROOT=$( echo -e ${QT_XKB_CONFIG_ROOT} | tr -s ":" "\n" | grep -vwE "(/opt/felix/bin/xkb)" | tr -s "\n" ":" | sed "s/:$//" )
  export LD_LIBRARY_PATH=$( echo -e ${LD_LIBRARY_PATH} | tr -s ":" "\n" | grep -vwE "(/opt/felix/lib)" | tr -s "\n" ":" | sed "s/:$//" )
  export FELIX_ROOT=$( echo -e ${FELIX_ROOT} | tr -s ":" "\n" | grep -vwE "(/opt/felix)" | tr -s "\n" ":" | sed "s/:$//" )

  export FLX_DIR=/afs/cern.ch/work/n/nswdaq/public/felix-sw/software_20200203/software/x86_64-centos7-gcc8-opt
  source /afs/cern.ch/work/n/nswdaq/public/felix-sw/software_20200203/software/cmake_tdaq/bin/setup.sh
  source /afs/cern.ch/user/n/nswdaq/.bash_flx_alias
else
  echo "Sourcing again the rpm-installed variables..."
  source /opt/felix/setup.sh
fi
