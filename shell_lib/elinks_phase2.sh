#!/bin/sh

if [[ $SETUP == *"191"* ]]; then
  IPL_L1P4_0=( 449     453     457     461     465     469     473     477     481     485    )
  IPL_L1P4_1=( 2497    2501    2505    2509    2513    2517    2521    2525    2529    2533)
  IPL_L1P5_0=( 513     517     521     525     529     533     537     541     545     549     )
  IPL_L1P5_1=( 2561    2565    2569    2573    2577    2581    2585    2589    2593    2597)
  IPL_L4P4_2=( 4097    4101    4105    4109    4113    4117    4121    4125    4129    4133    )
  IPL_L4P4_3=( 6145    6149    6153    6157    6161    6165    6169    6173    6177    6181)
  IPL_L4P5_2=( 4161    4165    4169    4173    4177    4181    4185    4189    4193    4197    )
  IPL_L4P5_3=( 6209    6213    6217    6221    6225    6229    6233    6237    6241    6245)
  HOL_L4P4_2=( 4225    4229    4233    4237    4241    4245    4249    4253    4257    4261    )
  HOL_L4P4_3=( 6273    6277    6281    6285    6289    6293    6297    6301    6305    6309)
  HOL_L4P5_2=( 4289    4293    4297    4301    4305    4309    4313    4317    4321    4325    )
  HOL_L4P5_3=( 6337    6341    6345    6349    6353    6357    6361    6365    6369    6373)
  HOL_L1P4_2=( 4353    4357    4361    4365    4369    4373    4377    4381    4385    4389    )
  HOL_L1P4_3=( 6401    6405    6409    6413    6417    6421    6425    6429    6433    6437)
  HOL_L1P5_2=( 4417    4421    4425    4429    4433    4437    4441    4445    4449    4453    )
  HOL_L1P5_3=( 6465    6469    6473    6477    6481    6485    6489    6493    6497    6501)
          
  IPR_L1P5_0=( 641     645     649     653     657     661     665     669     673     677     )
  IPR_L1P5_1=( 2689    2693    2697    2701    2705    2709    2713    2717    2721    2725)
  IPR_L1P4_0=( 705     709     713     717     721     725     729     733     737     741     )
  IPR_L1P4_1=( 2753    2757    2761    2765    2769    2773    2777    2781    2785    2789)
  IPR_L4P5_2=( 4481    4485    4489    4493    4497    4501    4505    4509    4513    4517    )
  IPR_L4P5_3=( 6529    6533    6537    6541    6545    6549    6553    6557    6561    6565)
  IPR_L4P4_2=( 4545    4549    4553    4557    4561    4565    4569    4573    4577    4581    )
  IPR_L4P4_3=( 6593    6597    6601    6605    6609    6613    6617    6621    6625    6629)
  HOR_L4P5_2=( 4609    4613    4617    4621    4625    4629    4633    4637    4641    4645    )
  HOR_L4P5_3=( 6657    6661    6665    6669    6673    6677    6681    6685    6689    6693)
  HOR_L4P4_2=( 4673    4677    4681    4685    4689    4693    4697    4701    4705    4709    )
  HOR_L4P4_3=( 6721    6725    6729    6733    6737    6741    6745    6749    6753    6757)
  HOR_L1P5_2=( 4737    4741    4745    4749    4753    4757    4761    4765    4769    4773    )
  HOR_L1P5_3=( 6785    6789    6793    6797    6801    6805    6809    6813    6817    6821)
  HOR_L1P4_2=( 4801    4805    4809    4813    4817    4821    4825    4829    4833    4837    )
  HOR_L1P4_3=( 6849    6853    6857    6861    6865    6869    6873    6877    6881    6885)
else

  L1_IPL_0=( 17 21 25 29 33 37 9 10 12 13 14 )
  L1_IPL_1=( 2305 2309 2313 2317 2321 2325 2329 2333 2337 2341 2369 2373 2377 2381 2385 2389 2393 2397 2401 2405 )

  L1_IPR_0=( 81 85 89 93 97 101 73 74 76 77 78 )
  L1_IPR_1=( 2433 2437 2441 2445 2449 2453 2457 2461 2465 2469 2497 2501 2505 2509 2513 2517 2521 2525 2529 2533 )

  L2_IPL_0=( 145 149 153 157 161 165 137 138 140 141 142)
  L2_IPL_1=( 2561 2565 2569 2573 2577 2581 2585 2589 2593 2597 2625 2629 2633 2637 2641 2645 2649 2653 2657 2661 )

  L2_IPR_0=( 209 213 217 221 225 229 201 202 204 205 206 )
  L2_IPR_1=( 2689 2693 2697 2701 2705 2709 2713 2717 2721 2725 2753 2757 2761 2765 2769 2773 2777 2781 2785 2789 )

  L3_IPL_0=( 273 277 281 285 289 293 265 266 268 269 270 )
  L3_IPL_2=( 4097 4101 4105 4109 4113 4117 4121 4125 4129 4133 4161 4165 4169 4173 4177 4181 4185 4189 4193 4197 )

  L3_IPR_0=( 337 341 345 349 353 357 329 330 332 333 334 )
  L3_IPR_2=( 4225 4229 4233 4237 4241 4245 4249 4253 4257 4261 4289 4293 4297 4301 4305 4309 4313 4317 4321 4325 )

  L4_IPL_0=( 401 405 409 413 417 421 393 394 396 397 398 )
  L4_IPL_2=( 4353 4357 4361 4365 4369 4373 4377 4381 4385 4389 4417 4421 4425 4429 4433 4437 4441 4445 4449 4453 )

  L4_IPR_0=( 465 469 473 477 481 485 457 458 460 461 462 )
  L4_IPR_2=( 4481 4485 4489 4493 4497 4501 4505 4509 4513 4517 4545 4549 4553 4557 4561 4565 4569 4573 4577 4581 )

  L1_HOL_0=( 529 533 537 541 545 549 521 522 524 525 526 )
  L1_HOL_2=( 4609 4613 4617 4621 4625 4629 4633 4637 4641 4645 4673 4677 4681 4685 4689 4693 4697 4701 4705 4709 )

  L1_HOR_0=( 593 597 601 605 609 613 585 586  588 589 590 )
  L1_HOR_2=( 4737 4741 4745 4749 4753 4757 4761 4765 4769 4773 4801 4805 4809 4813 4817 4821 4825 4829 4833 4837 )

  L2_HOL_0=( 657 661 665 669 673 677 649 650 652 653 654 )
  L2_HOL_3=( 6145 6149 6153 6157 6161 6165 6169 6173 6177 6181 6209 6213 6217 6221 6225 6229 6233 6237 6241 6245 )

  L2_HOR_0=( 721 725 729 733 737 741 713 714 716 717 718 )
  L2_HOR_3=( 6273 6277 6281 6285 6289 6293 6297 6301 6305 6309 6337 6341 6345 6349 6353 6357 6361 6365 6369 6373 )

  L3_HOL_1=( 2065 2069 2073 2077 2081 2085 2057 2058 2060 2061 2062 )
  L3_HOL_3=( 6401 6405 6409 6413 6417 6421 6425 6429 6433 6437 6465 6469 6473 6477 6481 6485 6489 6493 6497 6501 )

  L3_HOR_1=( 2129 2133 2137 2141 2145 2149 2121 2122 2124 2125 2126 )
  L3_HOR_3=( 6529 6533 6537 6541 6545 6549 6553 6557 6561 6565 6593 6597 6601 6605 6609 6613 6617 6621 6625 6629 )

  L4_HOL_1=( 2193 2197 2201 2205 2209 2213 2185 2186 2188 2189 2190 )
  L4_HOL_3=( 6657 6661 6665 6669 6673 6677 6681 6685 6689 6693 6721 6725 6729 6733 6737 6741 6745 6749 6753 6757 )

  L4_HOR_1=( 2257 2261 2265 2269 2273 2277 2249 2250 2252 2253 2254 )
  L4_HOR_3=( 6785 6789 6793 6797 6801 6805 6809 6813 6817 6821 6849 6853 6857 6861 6865 6869 6873 6877 6881 6885 )
fi


BOARDS_CARD0=()
BOARDS_CARD1=()
BOARDS_CARD2=()
BOARDS_CARD3=()

if [[ $1 == 0 ]]; then
  BOARDS_CARD0+=("${L1_IPL_0[@]}" "${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${L1_IPL_1[@]}" "${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${BOARDS_CARD3[@]}")
fi 

if [[ $1 == 1 ]]; then
  BOARDS_CARD0+=("${L1_IPR_0[@]}" "${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${L1_IPR_1[@]}" "${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${BOARDS_CARD3[@]}")
fi 

if [[ $1 == 2 ]]; then
  BOARDS_CARD0+=("${L2_IPL_0[@]}" "${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${L2_IPL_1[@]}" "${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${BOARDS_CARD3[@]}")
fi 

if [[ $1 == 3 ]]; then
  BOARDS_CARD0+=("${L2_IPR_0[@]}" "${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${L2_IPR_1[@]}" "${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${BOARDS_CARD3[@]}")
fi 

if [[ $1 == 4 ]]; then
  BOARDS_CARD0+=("${L3_IPL_0[@]}" "${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${L3_IPL_2[@]}" "${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${BOARDS_CARD3[@]}")
fi 

if [[ $1 == 5 ]]; then
  BOARDS_CARD0+=("${L3_IPR_0[@]}" "${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${L3_IPR_2[@]}" "${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${BOARDS_CARD3[@]}")
fi 

if [[ $1 == 6 ]]; then
  BOARDS_CARD0+=("${L4_IPL_0[@]}" "${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${L4_IPL_2[@]}" "${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${BOARDS_CARD3[@]}")
fi 

if [[ $1 == 7 ]]; then
  BOARDS_CARD0+=("${L4_IPR_0[@]}" "${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${L4_IPR_2[@]}" "${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${BOARDS_CARD3[@]}")
fi 

if [[ $1 == 8 ]]; then
  BOARDS_CARD0+=("${L1_HOL_0[@]}" "${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${L1_HOL_2[@]}" "${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${BOARDS_CARD3[@]}")
fi 

if [[ $1 == 9 ]]; then
  BOARDS_CARD0+=("${L1_HOR_0[@]}" "${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${L1_HOR_2[@]}" "${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${BOARDS_CARD3[@]}")
fi 

if [[ $1 == 10 ]]; then
  BOARDS_CARD0+=("${L2_HOL_0[@]}" "${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${L2_HOL_3[@]}" "${BOARDS_CARD3[@]}")
fi 

if [[ $1 == 11 ]]; then
  BOARDS_CARD0+=("${L2_HOR_0[@]}" "${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${L2_HOR_3[@]}" "${BOARDS_CARD3[@]}")
fi 

if [[ $1 == 12 ]]; then
  BOARDS_CARD0+=("${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${L3_HOL_1[@]}" "${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${L3_HOL_3[@]}" "${BOARDS_CARD3[@]}")
fi

if [[ $1 == 13 ]]; then
  BOARDS_CARD0+=("${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${L3_HOR_1[@]}" "${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${L3_HOR_3[@]}" "${BOARDS_CARD3[@]}")
fi

if [[ $1 == 14 ]]; then
  BOARDS_CARD0+=("${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${L4_HOL_1[@]}" "${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${L4_HOL_3[@]}" "${BOARDS_CARD3[@]}")
fi

if [[ $1 == 15 ]]; then
  BOARDS_CARD0+=("${BOARDS_CARD0[@]}")
  BOARDS_CARD1+=("${L4_HOR_1[@]}" "${BOARDS_CARD1[@]}")
  BOARDS_CARD2+=("${BOARDS_CARD2[@]}")
  BOARDS_CARD3+=("${L4_HOR_3[@]}" "${BOARDS_CARD3[@]}")
fi

ALL_BOARDS=()
ALL_BOARDS+=( "${BOARDS_CARD0[@]}" "${BOARDS_CARD1[@]}" "${BOARDS_CARD2[@]}" "${BOARDS_CARD3[@]}" )
