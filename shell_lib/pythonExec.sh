#!/bin/sh
DUMPS_ARRAY=()

# finds all instances of dumped files and puts them in an array
for entry in $DUMPS_LOC/dump_*.txt
do
  DUMPS_ARRAY+=($entry)
done

unset PYTHONPATH

#runs the python script for each dump. the $1 is the link
python $PYTHON_LOC $1 $(printf ' %s' "${DUMPS_ARRAY[@]}")
