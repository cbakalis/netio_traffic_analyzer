#!/bin/sh

export GBTX_CONF=/afs/cern.ch/user/n/nswdaq/public/felix-sw/l1ddc_config_files/MM_L1DDC_PHASE1_320_GBTX1_auto.xml
                                                                                                                                                                               
                                                                                                                                               
for link in {0..11}
do
  fice -d 0 -G $link -I 1 $GBTX_CONF
  echo "Done L1DDC at link $link card 0"
  echo "Waiting..."
  sleep 1
done

#191                                                                                                                                                                                          
if [[ $SETUP == *"191"* ]]; then

  for link in 6 7 9 10
  do
    fice -d 1 -G $link -I 1 $GBTX_CONF
    echo "Done L1DDC at link $link card 1"
    echo "Waiting..."
    sleep 1
  done

else

  #other, e.g. BB5. Please add more if needed                                                                                                                                                   
  for link in {0..3}
  do
    fice -d 1 -G $link -I 1 $GBTX_CONF
    echo "Done L1DDC at link $link card 1"
    echo "Waiting..."
    sleep 1
  done

fi
