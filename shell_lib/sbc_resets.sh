#!/bin/sh
source /afs/cern.ch/work/n/nswdaq/public/felix-sw/netio_traffic_analyzer/set_envVars.sh
source /afs/cern.ch/work/n/nswdaq/public/felix-sw/netio_traffic_analyzer/shell_lib/sbc_setup.sh || true

/afs/cern.ch/user/n/nswdaq/public/alti/ALTI_sr.expect  $ALTI_SLOT && sleep 2 && 
/afs/cern.ch/user/n/nswdaq/public/alti/ALTI_bcr.expect $ALTI_SLOT && sleep 2 && 
/afs/cern.ch/user/n/nswdaq/public/alti/ALTI_ecr.expect $ALTI_SLOT || true
