#!/bin/sh

echo "Removing the rpm-installed variables and sourcing the custom git-cloned software..."
export PATH=$( echo -e ${PATH} | tr -s ":" "\n" | grep -vwE "(/opt/felix/bin)" | tr -s "\n" ":" | sed "s/:$//" )
export QT_XKB_CONFIG_ROOT=$( echo -e ${QT_XKB_CONFIG_ROOT} | tr -s ":" "\n" | grep -vwE "(/opt/felix/bin/xkb)" | tr -s "\n" ":" | sed "s/:$//" )
export LD_LIBRARY_PATH=$( echo -e ${LD_LIBRARY_PATH} | tr -s ":" "\n" | grep -vwE "(/opt/felix/lib)" | tr -s "\n" ":" | sed "s/:$//" )
export FELIX_ROOT=$( echo -e ${FELIX_ROOT} | tr -s ":" "\n" | grep -vwE "(/opt/felix)" | tr -s "\n" ":" | sed "s/:$//" )