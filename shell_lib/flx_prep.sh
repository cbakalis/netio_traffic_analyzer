#!/bin/sh

# 191
if [ $SETUP == *"191"* ]; then
    
  for i in {0..3}
  do
    echo "$(tput setaf 7)Running FELIX E-Link configuration(SCA+TTC) for device $i at $HOSTNAME . $(tput sgr 0)"
    if [ $i -lt 2 ]; then
      feconf -d $i /afs/cern.ch/user/n/nswdaq/public/felix-sw/elinkconfig_files/fullWedge_191_partial_card$i\_SCA_TTC.elc
    else
      feconf -d $i /afs/cern.ch/user/n/nswdaq/public/felix-sw/elinkconfig_files/partial/link_none.elc
    fi

    femu -n -d $i -L
    flx-config -d $i set MMCM_MAIN_LCLK_SEL=0
    echo "Set Clock Configuration Source to TTC"
    echo "Done FELIX E-Link configuration(SCA+TTC) for device $i"
  done

# BB5
else
    for i in {0..3}
    do
      echo "$(tput setaf 7)Running FELIX E-Link configuration(SCA+TTC) for device $i at $HOSTNAME . $(tput sgr 0)"
      if [ $i -lt 2 ]; then
        feconf -d $i /afs/cern.ch/user/n/nswdaq/public/felix-sw/elinkconfig_files/fullWedge_bb5_partial_card$i\_SCA_TTC.elc
      else
        feconf -d $i /afs/cern.ch/user/n/nswdaq/public/felix-sw/elinkconfig_files/partial/link_none.elc
      fi
    
      femu -n -d $i -L 
      flx-config -d $i set MMCM_MAIN_LCLK_SEL=0
      echo "Set Clock Configuration Source to TTC"
      echo "Done FELIX E-Link configuration(SCA+TTC) for device $i"
    done
fi

echo "$FLX_DIR"

# is it new or is it old? the command of flx-init has changed
if [[ $(echo "$FLX_DIR" | grep "20200203") ]]; then
  flx-init -c 0
  flx-init -c 1
else
  flx-init -d 0
  flx-init -d 2
fi

