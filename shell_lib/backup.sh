#!/bin/sh

if [[ $PHASE == "PHASE1" ]]; then
  BACKUP_FILE=backup_logs/logBackup_$SECTOR\_$(date +"Date-Time-%Y_%m_%d-%H_%M_%S").txt
  STAMP_STR="$(date +"Backup created on %c. Please see the timestamps for each link to determine when the run was taken.")"
else
  BACKUP_FILE=backup_logs/logBackup_$PHASE\_$SECTOR\_$(date +"Date-Time-%Y_%m_%d-%H_%M_%S").txt
  STAMP_STR="$(date +"Backup created on %c. Please see the timestamps for each link to determine when the run was taken.")"
fi

if [ -f $LOG_LOC ]
then  
  cp $LOG_LOC $BACKUP_FILE
  sed -i "1 i $STAMP_STR" $BACKUP_FILE
fi
