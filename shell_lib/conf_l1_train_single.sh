#!/bin/sh

if [[ $1 -lt 12 ]]; then

    # now reset DLLs

    # group 1
    fice -d 0 -G $1 -I 1 -a 102 01
    fice -d 0 -G $1 -I 1 -a 103 01
    fice -d 0 -G $1 -I 1 -a 104 01

    # group 2
    fice -d 0 -G $1 -I 1 -a 126 00
    fice -d 0 -G $1 -I 1 -a 127 00
    fice -d 0 -G $1 -I 1 -a 128 00

    # group 3
    fice -d 0 -G $1 -I 1 -a 150 00
    fice -d 0 -G $1 -I 1 -a 151 00
    fice -d 0 -G $1 -I 1 -a 152 00

    # group 4
    fice -d 0 -G $1 -I 1 -a 174 00
    fice -d 0 -G $1 -I 1 -a 175 00
    fice -d 0 -G $1 -I 1 -a 176 00

    echo "Done L1DDC at link $1 card 0"
    echo "Waiting..."
    sleep 1
else

    LINK_CARD1=$(( $1 - 12 ))

    # group 1
    fice -d 1 -G $LINK_CARD1 -I 1 -a 102 01
    fice -d 1 -G $LINK_CARD1 -I 1 -a 103 01
    fice -d 1 -G $LINK_CARD1 -I 1 -a 104 01

    # group 2
    fice -d 1 -G $LINK_CARD1 -I 1 -a 126 00
    fice -d 1 -G $LINK_CARD1 -I 1 -a 127 00
    fice -d 1 -G $LINK_CARD1 -I 1 -a 128 00

    # group 3
    fice -d 1 -G $LINK_CARD1 -I 1 -a 150 00
    fice -d 1 -G $LINK_CARD1 -I 1 -a 151 00
    fice -d 1 -G $LINK_CARD1 -I 1 -a 152 00

    # group 4
    fice -d 1 -G $LINK_CARD1 -I 1 -a 174 00
    fice -d 1 -G $LINK_CARD1 -I 1 -a 175 00
    fice -d 1 -G $LINK_CARD1 -I 1 -a 176 00

    echo "Done L1DDC at link $1 card 1"
    echo "Waiting..."
    sleep 1
fi



    

