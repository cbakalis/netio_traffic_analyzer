#!/bin/sh
# using legacy config                                                                                                 
# export L1CT_INST_PATH=/afs/cern.ch/atlas/project/tdaq/level1/ctp/l1ct-08-03-03/installed
# export CMTCONFIG=x86_64-centos7-gcc8-opt
# source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-08-03-01 $CMTCONFIG
# source /afs/cern.ch/atlas/project/tdaq/level1/ctp/l1ct-08-03-03/installed/setup.sh $CMTCONFIG

# using latest config
export L1CT_INST_PATH=/afs/cern.ch/atlas/project/tdaq/level1/ctp/l1ct-08-03-05/installed
export CMTCONFIG=x86_64-centos7-gcc8-opt
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-08-03-01 $CMTCONFIG
source /afs/cern.ch/atlas/project/tdaq/level1/ctp/l1ct-08-03-05/installed/setup.sh $CMTCONFIG
