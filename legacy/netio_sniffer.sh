#!/bin/sh

FLX_HOST=pcatlnswfelix04

L1A_INFO=( 827 )
L1_IPL=( 16 20 24 28 33 37 9 10 12 13 )
L1_IPR=( 80 84 88 92 97 101 73 74 76 77 )
L2_IPL=( 144 148 152 156 161 165 137 138 140 141 )
L2_IPR=( 208 212 216 220 225 229 201 202 204 205 )
L3_IPL=( 272 276 280 284 289 293 265 266 268 269 )
L3_IPR=( 336 340 344 348 353 357 329 330 332 333 )
L4_IPL=( 412 404 408 400 417 421 393 394 396 397 )
L4_IPR=( 464 468 472 476 481 485 457 458 460 461 )

L1_HOL=( 528 532 536 540 545 549 521 522 524 525 )
L1_HOR=( 592 596 600 604 609 613 585 586 588 589 )
L2_HOL=( 656 660 664 668 673 677 649 650 652 653 )
L2_HOR=( 720 724 728 732 737 741 713 714 716 717 )

L3_HOL=( 2064 2068 2072 2076 2081 2085 2057 2058 2060 2061 )
L3_HOR=( 2128 2132 2136 2140 2145 2149 2121 2122 2124 2125 )
L4_HOL=( 2192 2196 2200 2204 2209 2213 2185 2186 2188 2189 )
L4_HOR=( 2256 2260 2264 2268 2273 2277 2249 2250 2252 2253 )

BOARDS_CARD0=()
BOARDS_CARD0+=(
"${L1A_INFO[@]}"
"${L1_IPL[@]}"
#"${L1_IPR[@]}"
#"${L2_IPL[@]}"
#"${L2_IPR[@]}"
#"${L3_IPL[@]}"
#"${L3_IPR[@]}"
#"${L4_IPL[@]}"
#"${L4_IPR[@]}"
#"${L1_HOL[@]}"
#"${L1_HOR[@]}"
#"${L2_HOL[@]}"
#"${L2_HOR[@]}"
"${BOARDS_CARD0[@]}")

BOARDS_CARD1=()
BOARDS_CARD1+=(
#"${L3_HOL[@]}"
#"${L3_HOR[@]}"
#"${L4_HOL[@]}"
#"${L4_HOR[@]}"
"${BOARDS_CARD1[@]}")

ALL_BOARDS=()
ALL_BOARDS+=( "${BOARDS_CARD0[@]}" "${BOARDS_CARD1[@]}" )

# cleanup
#if [ -f packets.txt ]; then
    rm -f dumps/dump_*
    rm -f packets.txt
#fi

for i in "${BOARDS_CARD0[@]}"
do
    netio_cat subscribe -H $FLX_HOST -e raw -p 12350 -t $i > dumps/dump_$i.txt&
done

for i in "${BOARDS_CARD1[@]}"
do
    netio_cat subscribe -H $FLX_HOST -e raw -p 12351 -t $i > dumps/dump_$i.txt&
done

while true
do
    echo 'Have the triggers been issued? If yes, do you want to continue with the checking? (please type y or n and press Enter).'
    read key

    if [ "$key" == 'y' ]; then
	echo -e "Continuing..."
    echo -e "Killing netio_cat..."
	killall netio_cat
    echo -e "Scanning the packets..."
	for i in "${ALL_BOARDS[@]}"
	do
	    echo "$i" `grep message dumps/dump_$i.txt | wc -l` > packets.txt
	done

	break
    elif [ "$key" == 'n' ]; then
        echo -e "Cancelling..."
        echo -e "Killing netio_cat..."
        killall netio_cat
	break
    fi
done

echo "Done!"


