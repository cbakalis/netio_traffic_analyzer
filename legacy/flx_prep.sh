#!/bin/sh

for i in {0..1}
do
    echo "$(tput setaf 7)Running FELIX E-Link configuration(SCA+ROC) for device $i at $HOSTNAME for BB5/MMG. $(tput sgr 0)"
    if [[ $1 -lt 12 ]]; then
        if [[ $i == 0 ]]; then
            feconf -d $i /afs/cern.ch/user/n/nswdaq/public/felix-sw/elinkconfig_files/partial/link_$1.elc
        fi 

        if [[ $i == 1 ]]; then
            feconf -d $i /afs/cern.ch/user/n/nswdaq/public/felix-sw/elinkconfig_files/partial/link_none.elc
        fi 

    else
        if [[ $i == 0 ]]; then
            feconf -d $i /afs/cern.ch/user/n/nswdaq/public/felix-sw/elinkconfig_files/partial/link_none.elc
        fi 

        if [[ $i == 1 ]]; then
            feconf -d $i /afs/cern.ch/user/n/nswdaq/public/felix-sw/elinkconfig_files/partial/link_$1.elc
        fi 
    fi
 	femu -n -d $i -L 
  	flx-config -d $i set MMCM_MAIN_LCLK_SEL=0
  	echo "Set Clock Configuration Source to TTC"
  	echo "Done FELIX E-Link configuration(SCA+ROC) for device $i"
done 

flx-init
