# netio_traffic_analyzer
A set of simple scripts that subscribe to a felixcore data-stream via netio_cat, and then analyze the inbound packet sizes. For E-link stability diagnostic purposes. The repo also contains scripts that change the ROC PLL core clock phases given the user's input and an MMFE8 configuration .json file. 

# Before Running
1. Make sure that you have the prerequisites in your host. See the `get_prerequisites.sh` before continuing
2. Make sure that the environmental variables reflect your setup. These can be found at `set_envVars.sh` . The default ones are for the BB5 setup
3. `$ source set_envVars.sh`

# How to Run
See https://docs.google.com/document/d/1ZbsFMMpkGa-JLu8sPoo1j_YnCLG_8f4gGjBbAKL9efw/edit#heading=h.bh15ikil9qv8

The related Section is: *"The ROC PLL Core Clock Calibration Procedure"*

## Contact
Questions, comments, suggestions, or help?

**Christos Bakalis**: <christos.bakalis@cern.ch>

